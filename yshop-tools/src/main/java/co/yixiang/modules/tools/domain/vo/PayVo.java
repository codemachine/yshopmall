package co.yixiang.modules.tools.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 发起支付
 * @author ocean
 */
@Data
@AllArgsConstructor
public class PayVo {
    /**
     * 商户订单号
     */
    String orderNo;
    /**
     * 支付方式
     */
    String wayCode;
    /**
     * 金额，单位分
     */
    Long amount;
    /**
     * 发起支付请求客户端的IP地址
     */
    String clientIp;
    /**
     * 商品标题
     */
    String subject;
    /**
     * 商品描述
     */
    String body;
    /**
     * 异步通知地址
     */
    String notifyUrl;
    /**
     * 前端跳转地址
     */
    String returnUrl;
    /**
     * 渠道扩展参数
     */
    String channelExtra;
    /**
     * 商户扩展参数,回调时原样返回
     */
    String extParam;

}
