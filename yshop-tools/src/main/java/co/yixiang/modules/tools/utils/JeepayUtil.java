package co.yixiang.modules.tools.utils;

import com.jeequan.jeepay.util.JeepayKit;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JeepayUtil {
    /**
     * 回调验签
     * @param map
     * @param apikey
     * @return
     */
    public static Boolean chackSgin(Map<String, Object> map, String apikey) {
        Object sign = map.remove("sign");
        String reSign = JeepayKit.getSign(map, apikey);

        if (!Objects.equals(reSign, sign)) {
            return true;
        }
        return false;
    }

    public static Map<String, Object> getParamsMap(HttpServletRequest req) {
        Map<String, String[]> requestMap = req.getParameterMap();
        Map<String, Object> paramsMap = new HashMap<>();
        requestMap.forEach((key, values) -> {
            String strs = "";
            for (String value : values) {
                strs = strs + value;
            }
            paramsMap.put(key, strs);
        });
        return paramsMap;
    }
}
