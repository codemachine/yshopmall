package co.yixiang.modules.tools.service;

import co.yixiang.modules.tools.config.JeepayConfig;
import co.yixiang.modules.tools.domain.vo.PayVo;
import co.yixiang.modules.tools.domain.vo.RefundVo;
import com.jeequan.jeepay.JeepayClient;
import com.jeequan.jeepay.exception.JeepayException;
import com.jeequan.jeepay.model.PayOrderCreateReqModel;
import com.jeequan.jeepay.model.RefundOrderCreateReqModel;
import com.jeequan.jeepay.request.PayOrderCreateRequest;
import com.jeequan.jeepay.request.RefundOrderCreateRequest;
import com.jeequan.jeepay.response.PayOrderCreateResponse;
import com.jeequan.jeepay.response.RefundOrderCreateResponse;
import org.springframework.stereotype.Service;

/**
 * @author ocean
 */
public interface JeePayService {

    /**
     * 支付下单
     * @param payDto
     * @return
     * @throws JeepayException
     */
    PayOrderCreateResponse unifiedOrder(PayVo payDto) throws JeepayException;

    /**
     * 支付退款
     * @param refundDto
     * @return
     * @throws JeepayException
     */
    RefundOrderCreateResponse refundOrder(RefundVo refundDto) throws JeepayException;

}
