package co.yixiang.modules.tools.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class JeepayConfig {
    @Value("${jeepay.appId}")
    public static String appId;
    @Value("${jeepay.apiKey}")
    public static String apiKey;
    @Value("${jeepay.mchNo}")
    public static String mchNo;
    @Value("${jeepay.apiBase}")
    public static String apiBase;
    @Value("${jeepay.notifyUrl}")
    public static String notifyUrl;
}
