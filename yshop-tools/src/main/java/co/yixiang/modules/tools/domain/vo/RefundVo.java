package co.yixiang.modules.tools.domain.vo;

import lombok.Data;

/**
 * 发起支付
 * @author ocean
 */
@Data
public class RefundVo {
    /**
     * 商户订单号
     */
    String orderNo;
    /**
     * 金额，单位分
     */
    Long amount;
    /**
     * 发起支付请求客户端的IP地址
     */
    String clientIp;
    /**
     * 退款原因
     */
    String refundReason;
    /**
     * 异步通知地址
     */
    String notifyUrl;
    /**
     * 渠道扩展参数
     */
    String channelExtra;
    /**
     * 商户扩展参数,回调时原样返回
     */
    String extParam;

}
