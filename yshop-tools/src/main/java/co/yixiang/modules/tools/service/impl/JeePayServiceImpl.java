package co.yixiang.modules.tools.service.impl;

import co.yixiang.modules.tools.config.JeepayConfig;
import co.yixiang.modules.tools.domain.vo.PayVo;
import co.yixiang.modules.tools.domain.vo.RefundVo;
import co.yixiang.modules.tools.service.JeePayService;
import com.jeequan.jeepay.JeepayClient;
import com.jeequan.jeepay.exception.JeepayException;
import com.jeequan.jeepay.model.PayOrderCreateReqModel;
import com.jeequan.jeepay.model.RefundOrderCreateReqModel;
import com.jeequan.jeepay.request.PayOrderCreateRequest;
import com.jeequan.jeepay.request.RefundOrderCreateRequest;
import com.jeequan.jeepay.response.PayOrderCreateResponse;
import com.jeequan.jeepay.response.RefundOrderCreateResponse;
import org.springframework.stereotype.Service;

/**
 * @author ocean
 */
@Service
public class JeePayServiceImpl implements JeePayService {

    /**
     * 支付下单
     * @param payDto
     * @return
     * @throws JeepayException
     */
    @Override
    public PayOrderCreateResponse unifiedOrder(PayVo payDto) throws JeepayException {
        // 创建客户端
        JeepayClient jeepayClient = JeepayClient.getInstance(JeepayConfig.appId, JeepayConfig.apiKey, JeepayConfig.apiBase);

        // 构建请求数据
        PayOrderCreateRequest request = new PayOrderCreateRequest();
        PayOrderCreateReqModel model = new PayOrderCreateReqModel();
        model.setMchNo(JeepayConfig.mchNo);
        model.setAppId(jeepayClient.getAppId());
        model.setMchOrderNo(payDto.getOrderNo());
        model.setWayCode(payDto.getWayCode());
        model.setAmount(payDto.getAmount());
        model.setCurrency("CNY");
        model.setClientIp(payDto.getClientIp());
        model.setSubject(payDto.getSubject());
        model.setBody(payDto.getBody());
        model.setNotifyUrl(payDto.getNotifyUrl());
        model.setReturnUrl(payDto.getReturnUrl());
        model.setChannelExtra(payDto.getChannelExtra());
        model.setExtParam(payDto.getExtParam());
        request.setBizModel(model);

        // 发起统一下单
        PayOrderCreateResponse response = jeepayClient.execute(request);
        return response;
    }

    /**
     * 支付退款
     * @param refundDto
     * @return
     * @throws JeepayException
     */
    @Override
    public RefundOrderCreateResponse refundOrder(RefundVo refundDto) throws JeepayException {
        // 创建客户端
        JeepayClient jeepayClient = JeepayClient.getInstance(JeepayConfig.appId, JeepayConfig.apiKey, JeepayConfig.apiBase);

        // 构建请求数据
        RefundOrderCreateRequest request = new RefundOrderCreateRequest();
        RefundOrderCreateReqModel model = new RefundOrderCreateReqModel();
        model.setMchNo(JeepayConfig.mchNo);
        model.setAppId(JeepayConfig.appId);
        model.setMchOrderNo(refundDto.getOrderNo());
        model.setMchRefundNo(refundDto.getOrderNo());
        model.setRefundAmount(refundDto.getAmount());
        model.setCurrency("cny");
        model.setClientIp(refundDto.getClientIp());
        model.setRefundReason(refundDto.getRefundReason());
        model.setNotifyUrl(refundDto.getNotifyUrl());
        model.setChannelExtra(refundDto.getChannelExtra());
        model.setExtParam(refundDto.getExtParam());
        request.setBizModel(model);

        // 发起统一退款
        RefundOrderCreateResponse response = jeepayClient.execute(request);
        return response;
    }

}
