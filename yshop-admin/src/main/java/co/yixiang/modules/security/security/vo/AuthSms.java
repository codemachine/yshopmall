/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co

 */
package co.yixiang.modules.security.security.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author liu
 */
@Getter
@Setter
public class AuthSms {
    @NotBlank(message = "手机号码不能为空")
    private String phone;
    private String code;
    private String uuid = "";

}
