/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co

 */
package co.yixiang.modules.security.security.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author hupeng
 * @date 2018-11-30
 */
@Getter
@Setter
public class RegisterUser {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private String phone;

    private String code;

    private String uuid = "";
    /**
     * 推广id
     */
    private Long spreadUid;

    @Override
    public String toString() {
        return "{username=" + username  + ", password= ******}";
    }
}
