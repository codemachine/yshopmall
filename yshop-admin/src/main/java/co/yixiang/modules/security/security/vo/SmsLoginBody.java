package co.yixiang.modules.security.security.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 短信登录对象
 *
 * @author Lion Li
 */

@Data
public class SmsLoginBody {

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String phonenumber;

    /**
     * 短信code
     */
    @NotBlank(message = "短信验证码不能为空")
    private String smsCode;

}
