/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co

 */
package co.yixiang.modules.security.service;

import co.yixiang.exception.BadRequestException;
import co.yixiang.modules.security.security.vo.JwtUser;
import co.yixiang.modules.system.domain.User;
import co.yixiang.modules.system.service.RoleService;
import co.yixiang.modules.system.service.UserService;
import co.yixiang.modules.system.service.dto.DeptSmallDto;
import co.yixiang.modules.system.service.dto.JobSmallDto;
import co.yixiang.modules.system.service.dto.UserDto;
import co.yixiang.modules.user.domain.YxUser;
import co.yixiang.modules.user.service.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author hupeng
 * @date 2018-11-22
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserLoginService {

    private final UserMapper yxUserMapper;


    public UserLoginService(UserMapper yxUserMapper) {
        this.yxUserMapper = yxUserMapper;
    }

    public UserDetails loadUserByPhoneNumber(String phoneNumber){
        YxUser user = yxUserMapper.selectOne(new LambdaQueryWrapper<YxUser>()
                .select(YxUser::getStatus, YxUser::getPhone, YxUser::getUsername, YxUser::getPassword)
                .eq(YxUser::getPhone, phoneNumber));

//        UserDto user = userService.findByName(username);
        if (user == null) {
            throw new BadRequestException("账号不存在");
        } else {
            if (user.getStatus()!=1) {
                throw new BadRequestException("账号未激活");
            }
            return createJwtUser(user);
        }
    }


    public UserDetails loadUserByUsername(String username) {
        YxUser user = yxUserMapper.selectOne(new LambdaQueryWrapper<YxUser>().select(YxUser::getStatus, YxUser::getPhone, YxUser::getUsername, YxUser::getPassword).eq(YxUser::getUsername, username));
        if (user == null) {
            throw new BadRequestException("账号不存在");
        } else {
            if (user.getStatus()!=1) {
                throw new BadRequestException("账号未激活");
            }
            return createJwtUser(user);
        }
    }

    private UserDetails createJwtUser(YxUser user) {
        return new JwtUser(
                user.getUid(),
                user.getUsername(),
                user.getRealName(),
                null,
                user.getPassword(),
                user.getAvatar(),
                null,
                user.getPhone(),
                null,
                null,
                null,
                user.getStatus() == 1,
                null,
                null
        );
    }
}
