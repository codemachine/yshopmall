/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co

 */
package co.yixiang.modules.order.rest;

import co.yixiang.annotation.AnonymousAccess;
import co.yixiang.enums.PayTypeEnum;
import co.yixiang.modules.logging.aop.log.Log;
import co.yixiang.modules.order.service.YxStoreOrderService;
import co.yixiang.modules.tools.config.JeepayConfig;
import co.yixiang.modules.tools.domain.AlipayConfig;
import co.yixiang.modules.tools.domain.vo.TradeVo;
import co.yixiang.modules.tools.service.AlipayConfigService;
import co.yixiang.modules.tools.utils.AliPayStatusEnum;
import co.yixiang.modules.tools.utils.AlipayUtils;
import co.yixiang.modules.tools.utils.JeepayUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author hupeng
 * @date 2018-12-31
 */
@Slf4j
@RestController
@RequestMapping("/api/jeepay")
@Api(tags = "工具：jeepay")
public class JeePayController {
    private final YxStoreOrderService yxStoreOrderService;

    public JeePayController(YxStoreOrderService yxStoreOrderService){
        this.yxStoreOrderService = yxStoreOrderService;
    }
    @ApiIgnore
    @RequestMapping("/notify")
    @AnonymousAccess
    @SuppressWarnings("all")
    @ApiOperation("支付异步通知(要公网访问)")
    public String jeeapyNotify(HttpServletRequest request){
        Map<String, Object> map = JeepayUtil.getParamsMap(request);
        //验签
        if (JeepayUtil.chackSgin(map, JeepayConfig.apiKey)) {
            return "failure";
        }else {
            //订单号
            String orderNumber = map.get("mchOrderNo").toString();
            String wayCode = map.get("wayCode").toString();
            //支付金额
            BigDecimal amount = new BigDecimal(map.get("amount").toString()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
            //验证通过后应该根据业务需要处理订单
            //支付成功后处理

            yxStoreOrderService.paySuccess(orderNumber, wayCode);

            //返回成功
            return "success";
        }
    }

}
