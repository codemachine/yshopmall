/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.category.rest;
import java.util.Arrays;
import co.yixiang.dozer.service.IGenerator;
import co.yixiang.modules.category.domain.NftCategory;
import co.yixiang.modules.category.service.dto.NftCategoryDto;
import lombok.AllArgsConstructor;
import co.yixiang.modules.logging.aop.log.Log;
import co.yixiang.modules.category.service.NftCategoryService;
import co.yixiang.modules.category.service.dto.NftCategoryQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import co.yixiang.domain.PageResult;
/**
* @author liu
* @date 2023-12-19
*/
@AllArgsConstructor
@Api(tags = "nftCategory管理")
@RestController
@RequestMapping("/api/nftCategory")
public class NftCategoryController {

    private final NftCategoryService nftCategoryService;
    private final IGenerator generator;


    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','nftCategory:list')")
    public void download(HttpServletResponse response, NftCategoryQueryCriteria criteria) throws IOException {
        nftCategoryService.download(generator.convert(nftCategoryService.queryAll(criteria), NftCategoryDto.class), response);
    }

    @GetMapping
    @Log("查询nftCategory")
    @ApiOperation("查询nftCategory")
    @PreAuthorize("@el.check('admin','nftCategory:list')")
    public ResponseEntity<PageResult<NftCategoryDto>> getNftCategorys(NftCategoryQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(nftCategoryService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增nftCategory")
    @ApiOperation("新增nftCategory")
    @PreAuthorize("@el.check('admin','nftCategory:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody NftCategory resources){
        return new ResponseEntity<>(nftCategoryService.save(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改nftCategory")
    @ApiOperation("修改nftCategory")
    @PreAuthorize("@el.check('admin','nftCategory:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody NftCategory resources){
        nftCategoryService.updateById(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除nftCategory")
    @ApiOperation("删除nftCategory")
    @PreAuthorize("@el.check('admin','nftCategory:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Long[] ids) {
        Arrays.asList(ids).forEach(id->{
            nftCategoryService.removeById(id);
        });
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
