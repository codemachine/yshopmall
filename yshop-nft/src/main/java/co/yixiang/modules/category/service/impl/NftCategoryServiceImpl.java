/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.category.service.impl;

import co.yixiang.modules.category.domain.NftCategory;
import co.yixiang.modules.category.service.dto.NftCategoryDto;
import co.yixiang.modules.category.service.dto.NftCategoryQueryCriteria;
import co.yixiang.common.service.impl.BaseServiceImpl;
import lombok.AllArgsConstructor;
import co.yixiang.dozer.service.IGenerator;
import com.github.pagehelper.PageInfo;
import co.yixiang.common.utils.QueryHelpPlus;
import co.yixiang.utils.FileUtil;
import co.yixiang.modules.category.service.NftCategoryService;
import co.yixiang.modules.category.service.mapper.NftCategoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import co.yixiang.domain.PageResult;
/**
* @author liu
* @date 2023-12-19
*/
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "nftCategory")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class NftCategoryServiceImpl extends BaseServiceImpl<NftCategoryMapper, NftCategory> implements NftCategoryService {

    private final IGenerator generator;

    @Override
    //@Cacheable
    public PageResult<NftCategoryDto> queryAll(NftCategoryQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<NftCategory> page = new PageInfo<>(queryAll(criteria));
        return generator.convertPageInfo(page,NftCategoryDto.class);
    }


    @Override
    //@Cacheable
    public List<NftCategory> queryAll(NftCategoryQueryCriteria criteria){
        return baseMapper.selectList(QueryHelpPlus.getPredicate(NftCategory.class, criteria));
    }


    @Override
    public void download(List<NftCategoryDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (NftCategoryDto nftCategory : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("上机分类的编号：0表示一级分类", nftCategory.getParentId());
            map.put("nft名称", nftCategory.getName());
            map.put("分类级别：0->1级；1->2级", nftCategory.getLevel());
            map.put("显示状态：0->不显示；1->显示", nftCategory.getShowStatus());
            map.put("排序", nftCategory.getSort());
            map.put("图标", nftCategory.getIcon());
            map.put("创建人", nftCategory.getCreateBy());
            map.put("创建时间", nftCategory.getCreateTime());
            map.put("修改人", nftCategory.getUpdateBy());
            map.put("修改时间", nftCategory.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
