/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.category.service.dto;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;

/**
* @author liu
* @date 2023-12-19
*/
@Data
public class NftCategoryDto implements Serializable {

    private Long id;

    /** 上机分类的编号：0表示一级分类 */
    private Long parentId;

    /** nft名称 */
    private String name;

    /** 分类级别：0->1级；1->2级 */
    private Integer level;

    /** 显示状态：0->不显示；1->显示 */
    private Integer showStatus;

    /** 排序 */
    private Integer sort;

    /** 图标 */
    private String icon;

    /** 创建人 */
    private Long createBy;

    /** 创建时间 */
    private Date createTime;

    /** 修改人 */
    private Long updateBy;

    /** 修改时间 */
    private Date updateTime;
}
