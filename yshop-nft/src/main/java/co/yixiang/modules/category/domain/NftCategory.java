/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.category.domain;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import co.yixiang.domain.BaseDomain;

/**
* @author liu
* @date 2023-12-19
*/
@Data
@TableName("nft_category")
public class NftCategory extends BaseDomain {
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 上机分类的编号：0表示一级分类 */
    private Long parentId;

    /** nft名称 */
    private String name;

    /** 分类级别：0->1级；1->2级 */
    private Integer level;

    /** 显示状态：0->不显示；1->显示 */
    private Integer showStatus;

    /** 排序 */
    private Integer sort;

    /** 图标 */
    private String icon;

    /** 创建人 */
    private Long createBy;


    /** 修改人 */
    private Long updateBy;



    public void copy(NftCategory source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
