/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.nft.service.dto;

import lombok.Data;
import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;

/**
* @author liu
* @date 2023-12-19
*/
@Data
public class NftDto implements Serializable {

    private Long nftId;

    private Long productSnapshotId;

    /** 活动 */
    private Long brandId;

    /** 分类id */
    private Long categoryId;

    /** nft编码 */
    private String nftCode;

    /** nft名称 */
    private String name;

    /** 主图 */
    private String pic;

    /** 画册图片，连产品图片限制为5张，以逗号分割 */
    private String albumPics;

    /** 上架状态：0->下架；1->上架 */
    private Integer publishStatus;

    /** 排序 */
    private Integer sort;

    /** 金额 */
    private BigDecimal price;

    /** 商品重量，默认为克 */
    private BigDecimal weight;

    /** 产品详情网页内容 */
    private String detailHtml;

    /** 移动端网页详情 */
    private String detailMobileHtml;

    /** 品牌名称 */
    private String brandName;

    /** nft分类名称 */
    private String nftCategoryName;

    /** 创建人 */
    private Long createBy;

    /** 创建时间 */
    private Date createTime;

    /** 修改人 */
    private Long updateBy;

    /** 修改时间 */
    private Date updateTime;

    /** nft属性，json格式 */
    private String productAttr;
}
