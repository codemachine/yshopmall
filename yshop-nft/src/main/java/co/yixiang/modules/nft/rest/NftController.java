/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.nft.rest;
import java.util.ArrayList;
import java.util.Arrays;
import co.yixiang.dozer.service.IGenerator;
import co.yixiang.modules.nft.domain.Nft;
import co.yixiang.modules.nft.service.NftService;
import co.yixiang.modules.nft.service.dto.NftDto;
import co.yixiang.modules.nft.service.dto.NftQueryCriteria;
import lombok.AllArgsConstructor;
import co.yixiang.modules.logging.aop.log.Log;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import co.yixiang.domain.PageResult;
/**
* @author liu
* @date 2023-12-19
*/
@AllArgsConstructor
@Api(tags = "nft管理")
@RestController
@RequestMapping("api/nft")
public class NftController {

    private final NftService nftService;
    private final IGenerator generator;


    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','nft:list')")
    public void download(HttpServletResponse response, NftQueryCriteria criteria) throws IOException {
        nftService.download(generator.convert(nftService.queryAll(criteria), NftDto.class), response);
    }

    @GetMapping
    @Log("查询nft")
    @ApiOperation("查询nft")
    @PreAuthorize("@el.check('admin','nft:list')")
    public ResponseEntity<PageResult<NftDto>> getNfts(NftQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(nftService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增nft")
    @ApiOperation("新增nft")
    @PreAuthorize("@el.check('admin','nft:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Nft resources){
        Integer number = resources.getNumber();
        List<Nft> nfts = new ArrayList<>();
        if (number <= 0){

        }else {
            for (Integer i = 0; i < number; i++) {
                nfts.add(resources);
            }
        }
        boolean b = nftService.saveBatch(nfts);
        return new ResponseEntity<>(b,HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改nft")
    @ApiOperation("修改nft")
    @PreAuthorize("@el.check('admin','nft:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Nft resources){
        nftService.updateById(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除nft")
    @ApiOperation("删除nft")
    @PreAuthorize("@el.check('admin','nft:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Long[] ids) {
        Arrays.asList(ids).forEach(id->{
            nftService.removeById(id);
        });
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
