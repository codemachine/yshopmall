/**
* Copyright (C) 2018-2022
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制，未经购买不得使用
* 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
* 一经发现盗用、分享等行为，将追究法律责任，后果自负
*/
package co.yixiang.modules.nft.service.impl;

import co.yixiang.modules.nft.service.NftService;
import co.yixiang.modules.nft.service.dto.NftDto;
import co.yixiang.modules.nft.service.dto.NftQueryCriteria;
import co.yixiang.modules.nft.service.mapper.NftMapper;
import co.yixiang.modules.nft.domain.Nft;
import co.yixiang.common.service.impl.BaseServiceImpl;
import lombok.AllArgsConstructor;
import co.yixiang.dozer.service.IGenerator;
import com.github.pagehelper.PageInfo;
import co.yixiang.common.utils.QueryHelpPlus;
import co.yixiang.utils.FileUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import co.yixiang.domain.PageResult;
/**
* @author liu
* @date 2023-12-19
*/
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "nft")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class NftServiceImpl extends BaseServiceImpl<NftMapper, Nft> implements NftService {

    private final IGenerator generator;

    @Override
    //@Cacheable
    public PageResult<NftDto> queryAll(NftQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<Nft> page = new PageInfo<>(queryAll(criteria));
        return generator.convertPageInfo(page,NftDto.class);
    }


    @Override
    //@Cacheable
    public List<Nft> queryAll(NftQueryCriteria criteria){
        return baseMapper.selectList(QueryHelpPlus.getPredicate(Nft.class, criteria));
    }


    @Override
    public void download(List<NftDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (NftDto nft : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put(" productSnapshotId",  nft.getProductSnapshotId());
            map.put("活动", nft.getBrandId());
            map.put("分类id", nft.getCategoryId());
            map.put("nft编码", nft.getNftCode());
            map.put("nft名称", nft.getName());
            map.put("主图", nft.getPic());
            map.put("画册图片，连产品图片限制为5张，以逗号分割", nft.getAlbumPics());
            map.put("上架状态：0->下架；1->上架", nft.getPublishStatus());
            map.put("排序", nft.getSort());
            map.put("金额", nft.getPrice());
            map.put("商品重量，默认为克", nft.getWeight());
            map.put("产品详情网页内容", nft.getDetailHtml());
            map.put("移动端网页详情", nft.getDetailMobileHtml());
            map.put("品牌名称", nft.getBrandName());
            map.put("nft分类名称", nft.getNftCategoryName());
            map.put("创建人", nft.getCreateBy());
            map.put("创建时间", nft.getCreateTime());
            map.put("修改人", nft.getUpdateBy());
            map.put("修改时间", nft.getUpdateTime());
            map.put("nft属性，json格式", nft.getProductAttr());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
