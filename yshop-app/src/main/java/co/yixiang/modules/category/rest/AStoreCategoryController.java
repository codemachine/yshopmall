/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co
 * 注意：
 * 本软件为www.yixiang.co开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package co.yixiang.modules.category.rest;

import co.yixiang.modules.category.service.YxStoreCategoryService;
import co.yixiang.utils.CateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
* @author hupeng
* @date 2019-10-03
*/
@Api(tags = "APP商城:商品分类")
@RestController
@RequestMapping("/app/api")
public class AStoreCategoryController {


    private final YxStoreCategoryService yxStoreCategoryService;


    public AStoreCategoryController(YxStoreCategoryService yxStoreCategoryService) {
        this.yxStoreCategoryService = yxStoreCategoryService;
    }

    @ApiOperation(value = "获取分类列表树形列表")
    @GetMapping(value = "/yxStoreCategory")
    public ResponseEntity getYxStoreCategorys(){
        List<CateDTO> list = yxStoreCategoryService.getList();
        return new ResponseEntity<>(list,HttpStatus.OK);
    }
}
