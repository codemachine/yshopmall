package co.yixiang.modules.domain.Vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreCartVo {
    @ApiModelProperty("普通产品编号")
    Long productId;
    @ApiModelProperty("购物车数量")
    Integer cartNum;
    @ApiModelProperty("属性唯一值")
    String productAttrUnique;
    @ApiModelProperty("1 加入购物车直接购买  0 加入购物车")
    Integer isNew;
    @ApiModelProperty("拼团id")
    Long combinationId;
    @ApiModelProperty("秒杀id")
    Long seckillId;
    @ApiModelProperty("砍价id")
    Long bargainId;
}
