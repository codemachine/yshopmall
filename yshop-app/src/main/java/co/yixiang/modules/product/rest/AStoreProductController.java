/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co
 * 注意：
 * 本软件为www.yixiang.co开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package co.yixiang.modules.product.rest;

import co.yixiang.constant.ShopConstants;
import co.yixiang.modules.aop.ForbidSubmit;
import co.yixiang.modules.logging.aop.log.Log;
import co.yixiang.modules.product.param.YxStoreProductQueryParam;
import co.yixiang.modules.product.service.YxStoreProductService;
import co.yixiang.modules.product.service.dto.StoreProductDto;
import co.yixiang.modules.product.vo.YxStoreProductQueryVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author hupeng
 * @date 2019-10-04
 */
@Api(tags = "APP商城:商品")
@RestController
@RequestMapping("/app/api")
public class AStoreProductController {

    private final YxStoreProductService yxStoreProductService;
    public AStoreProductController(YxStoreProductService yxStoreProductService) {
        this.yxStoreProductService = yxStoreProductService;
    }

    /**
     * 商品列表
     * @param productQueryParam YxStoreProductQueryParam
     * @return list
     */
    @ApiOperation(value = "查询商品")
    @GetMapping(value = "/getGoodsList")
    public ResponseEntity getGoodsList(YxStoreProductQueryParam productQueryParam){
        List<YxStoreProductQueryVo> goodsList = yxStoreProductService.getGoodsList(productQueryParam);
        return new ResponseEntity<>(goodsList,HttpStatus.OK);
    }

    /**
     * 商品详情
     * @param id 商品id
     * @param uid 用户id
     * @param latitude 纬度
     * @param longitude 经度
     * @return ProductVo
     */
    @ApiOperation(value = "商品详情")
    @GetMapping(value = "/goodsDetail")
    public ResponseEntity goodsDetail(Long id, Long uid, String latitude, String longitude){
        return new ResponseEntity<>(yxStoreProductService.goodsDetail(id, uid, latitude, longitude),HttpStatus.OK);
    }

    /**
     *  商品浏览
     * @param productId
     */
    @ApiOperation(value = "商品浏览")
    @GetMapping(value = "/incBrowseNum")
    public ResponseEntity incBrowseNum(Long productId){
        yxStoreProductService.incBrowseNum(productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ForbidSubmit
    @Log("新增/修改商品")
    @ApiOperation(value = "新增/修改商品")
    @CacheEvict(cacheNames = ShopConstants.YSHOP_REDIS_INDEX_KEY,allEntries = true)
    @PostMapping(value = "/yxStoreProduct/addOrSave")
    public ResponseEntity create(@Validated @RequestBody StoreProductDto storeProductDto){
        yxStoreProductService.insertAndEditYxStoreProduct(storeProductDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @ForbidSubmit
    @Log("删除商品")
    @ApiOperation(value = "删除商品")
    @CacheEvict(cacheNames = ShopConstants.YSHOP_REDIS_INDEX_KEY,allEntries = true)
    @DeleteMapping(value = "/yxStoreProduct/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        yxStoreProductService.removeById(id);
        return new ResponseEntity(HttpStatus.OK);
    }



    @ApiOperation(value = "商品上架/下架")
    @CacheEvict(cacheNames = ShopConstants.YSHOP_REDIS_INDEX_KEY,allEntries = true)
    @PostMapping(value = "/yxStoreProduct/onsale/{id}")
    public ResponseEntity onSale(@PathVariable Long id,@RequestBody String jsonStr){
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        Integer status = jsonObject.getInteger("status");
        yxStoreProductService.onSale(id,status);
        return new ResponseEntity(HttpStatus.OK);
    }
}
