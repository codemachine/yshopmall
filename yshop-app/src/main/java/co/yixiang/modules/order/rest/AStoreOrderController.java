/**
 * Copyright (C) 2018-2022
 * All rights reserved, Designed By www.yixiang.co
 * 注意：
 * 本软件为www.yixiang.co开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package co.yixiang.modules.order.rest;

import cn.hutool.core.util.IdUtil;
import co.yixiang.dozer.service.IGenerator;
import co.yixiang.modules.aop.ForbidSubmit;
import co.yixiang.modules.order.domain.YxStoreOrder;
import co.yixiang.modules.order.param.OrderParam;
import co.yixiang.modules.order.service.YxStoreOrderService;
import co.yixiang.modules.order.service.YxStoreOrderStatusService;
import co.yixiang.modules.order.vo.ConfirmOrderVo;
import co.yixiang.modules.order.vo.YxStoreOrderQueryVo;
import co.yixiang.modules.tools.config.JeepayConfig;
import co.yixiang.modules.tools.domain.vo.PayVo;
import co.yixiang.modules.tools.service.JeePayService;
import co.yixiang.modules.user.domain.YxUser;
import co.yixiang.utils.SecurityUtils;
import co.yixiang.utils.StringUtils;
import com.jeequan.jeepay.exception.JeepayException;
import com.jeequan.jeepay.response.PayOrderCreateResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author hupeng
 * @date 2019-10-14
 */
@Api(tags = "APP商城:订单")
@RestController
@RequestMapping("/app/api/order")
@Slf4j
@SuppressWarnings("unchecked")
public class AStoreOrderController {


    private final IGenerator generator;
    private final YxStoreOrderStatusService yxStoreOrderStatusService;
    private final YxStoreOrderService yxStoreOrderService;
    private final JeePayService jeePayService;


    public AStoreOrderController(IGenerator generator, YxStoreOrderService yxStoreOrderService,
                                 YxStoreOrderStatusService yxStoreOrderStatusService,
                                 JeePayService jeePayService) {
        this.generator = generator;
        this.yxStoreOrderService = yxStoreOrderService;
        this.yxStoreOrderStatusService = yxStoreOrderStatusService;
        this.jeePayService = jeePayService;
    }

    /**
     * 订单评价
     */
//    @ForbidSubmit
//    @PostMapping("/orderComment")
//    @ApiOperation(value = "订单评价",notes = "订单评价")
//    public ResponseEntity orderComment(Long oid, Long productId, YxUser user, String unique, String comment, String pics, String productScore,
//                      String serviceScore){
//        yxStoreOrderService.orderComment(oid, productId, user, unique, comment, pics, productScore, serviceScore);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

    /**
     * 返回订单确认数据
     * @param cartIds 购物车id
     * @return ConfirmOrderVO
     */
    @GetMapping("/orderComment/{cartIds}")
    @ApiOperation(value = "返回订单确认数据",notes = "返回订单确认数据",response = ConfirmOrderVo.class)
    public ResponseEntity confirmOrder(@PathVariable("cartIds") String cartIds){
        ConfirmOrderVo confirmOrderVo = yxStoreOrderService.confirmOrder(new YxUser(){{setUid(SecurityUtils.getUserId());}}, cartIds);
        return new ResponseEntity<>(confirmOrderVo,HttpStatus.OK);
    }

    /**
     * 删除订单
     * @param orderId 单号
     */
    @ForbidSubmit
    @DeleteMapping("/orderComment/{cartIds}")
    @ApiOperation(value = "删除订单",notes = "删除订单")
    public ResponseEntity removeOrder(String orderId){
        yxStoreOrderService.removeOrder(orderId, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 订单确认收货
     * @param orderId 单号
     */
    @ForbidSubmit
    @PostMapping("/takeOrder/{orderId}")
    @ApiOperation(value = "订单确认收货",notes = "订单确认收货")
    public ResponseEntity takeOrder(@PathVariable("orderId") String orderId){
        yxStoreOrderService.takeOrder(orderId, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 申请退款
     * @param explain 退款备注
     * @param img 图片
     * @param text 理由
     * @param orderId 订单号
     */
    @PostMapping("/orderApplyRefund")
    @ApiOperation(value = "申请退款",notes = "申请退款")
    @ForbidSubmit
    public ResponseEntity orderApplyRefund(String explain,String img,String text,String orderId){
        yxStoreOrderService.orderApplyRefund(explain, img, text, orderId, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 未付款取消订单
     * @param orderId 订单号
     */
    @ForbidSubmit
    @PostMapping("/cancelOrder/{orderId}")
    @ApiOperation(value = "未付款取消订单",notes = "未付款取消订单")
    public ResponseEntity cancelOrder(@PathVariable("orderId") String orderId){
        yxStoreOrderService.cancelOrder(orderId, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 订单列表
     * @param type OrderStatusEnum
     * @param page page
     * @param limit limit
     * @return list
     */
    @GetMapping("/orderList")
    @ApiOperation(value = "订单列表",notes = "订单列表")
    public ResponseEntity orderList(int type,int page,int limit){
        Map<String, Object> stringObjectMap = yxStoreOrderService.orderList(SecurityUtils.getUserId(), type, page, limit);
        return new ResponseEntity<>(stringObjectMap, HttpStatus.OK);
    }

    /**
     * 余额支付
     * @param orderId 订单号
     */
    @ForbidSubmit
    @PostMapping("/yuePay/{orderId}")
    @ApiOperation(value = "余额支付",notes = "余额支付")
    public ResponseEntity yuePay(@PathVariable("orderId") String orderId){
        yxStoreOrderService.yuePay(orderId, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 积分兑换
     * @param orderId 订单号
     */
    @ForbidSubmit
    @PostMapping("/integralPay/{orderId}")
    @ApiOperation(value = "积分兑换",notes = "积分兑换")
    public ResponseEntity integralPay(@PathVariable("orderId") String orderId){
        yxStoreOrderService.integralPay(orderId, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 支付下单
     *
     * @param orderId,支付宝支付
     * @return
     */
    @ForbidSubmit
    @GetMapping("/jeepay/{orderId}")
    @ApiOperation(value = "支付宝支付",notes = "支付下单")
    public ResponseEntity aliPay(@PathVariable("orderId") String orderId, @RequestParam("wayCode") String wayCode, HttpServletRequest request) throws JeepayException {
        YxStoreOrderQueryVo orderInfo = yxStoreOrderService.getOrderInfo(orderId, SecurityUtils.getUserId());
        PayVo payDto = new PayVo(orderInfo.getOrderId(),
                wayCode,
                orderInfo.getTotalPrice().longValue() * 100,
                StringUtils.getIp(request),
                "商品购买","商品购买", JeepayConfig.notifyUrl,null,null,null);
        PayOrderCreateResponse s = jeePayService.unifiedOrder(payDto);
        return new ResponseEntity<>(s, HttpStatus.OK);
    }

    /**
     * 创建订单
     * @param param param
     * @return YxStoreOrder
     */
    @ForbidSubmit
    @PostMapping("/createOrder")
    @ApiOperation(value = "创建订单",notes = "创建订单",response = OrderParam.class)
    public ResponseEntity createOrder(OrderParam param, String orderKey){
        YxStoreOrder order = yxStoreOrderService.createOrder(new YxUser() {{
            setUid(SecurityUtils.getUserId());
        }}, orderKey, param);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

}
