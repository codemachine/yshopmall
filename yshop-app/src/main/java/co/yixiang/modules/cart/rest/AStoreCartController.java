package co.yixiang.modules.cart.rest;

import co.yixiang.modules.cart.service.YxStoreCartService;
import co.yixiang.modules.cart.service.dto.YxStoreCartQueryCriteria;
import co.yixiang.modules.domain.Vo.StoreCartVo;
import co.yixiang.modules.logging.aop.log.Log;
import co.yixiang.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "APP商城:购物车")
@RestController
@RequestMapping("/app/api/cart")
public class AStoreCartController {

    private final YxStoreCartService yxStoreCartServic;

    public AStoreCartController(YxStoreCartService yxStoreCartServic){
        this.yxStoreCartServic=yxStoreCartServic;
    }

    /**
     * 删除购物车
     * @param uid uid
     * @param ids 购物车id集合
     */
    @Log("删除购物车")
    @ApiOperation(value = "删除购物车")
    @DeleteMapping(value = "/removeUserCart")
    public ResponseEntity removeUserCart(Long uid, List<String> ids){
        yxStoreCartServic.removeUserCart(SecurityUtils.getUserId(), ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 改购物车数量
     * @param cartId 购物车id
     * @param cartNum 数量
     */
    @Log("改购物车数量")
    @ApiOperation(value = "改购物车数量")
    @PutMapping(value = "/changeUserCartNum")
    public ResponseEntity changeUserCartNum(Long cartId,int cartNum) {
        yxStoreCartServic.changeUserCartNum(cartId, cartNum, SecurityUtils.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);

    }

    /**
     * 购物车列表
     * @param cartIds 购物车id，多个逗号隔开
     * @param status 0-购购物车列表
     * @return map valid-有效购物车 invalid-失效购物车
     */
    @Log("购物车列表")
    @ApiOperation(value = "购物车列表")
    @GetMapping(value = "/getUserProductCartList")
    public ResponseEntity getUserProductCartList(String cartIds, Integer status) {
        return new ResponseEntity<>(yxStoreCartServic.getUserProductCartList(SecurityUtils.getUserId(), cartIds, status), HttpStatus.OK);
    }

    /**
     * 返回当前用户购物车总数量
     * @param uid 用户id
     * @return int
     */
    @ApiOperation(value = "返回当前用户购物车总数量")
    @GetMapping(value = "/getUserCartNum")
    public ResponseEntity getUserCartNum(Long uid) {
        return new ResponseEntity<>(yxStoreCartServic.getUserCartNum(SecurityUtils.getUserId()), HttpStatus.OK);
    }

    /**
     * 添加购物车
     * @return 购物车id
     */
    @Log("添加购物车")
    @ApiOperation(value = "添加购物车")
    @PutMapping(value = "/addCart")
    public ResponseEntity addCart(@RequestBody StoreCartVo storeCartVo) {
        return new ResponseEntity<>(yxStoreCartServic.addCart(SecurityUtils.getUserId(),
                storeCartVo.getProductId(),storeCartVo.getCartNum(),storeCartVo.getProductAttrUnique(),
                storeCartVo.getIsNew(),storeCartVo.getCombinationId(),storeCartVo.getSeckillId(),storeCartVo.getBargainId()), HttpStatus.OK);
    }

    /**
     * 查询数据分页
     * @param criteria 条件
     * @param pageable 分页参数
     * @return Map<String,Object>
     */
    @ApiOperation(value = "添加购物车")
    @PutMapping(value = "/queryAll")
    public ResponseEntity queryAll(YxStoreCartQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity<>(yxStoreCartServic.queryAll(criteria, pageable), HttpStatus.OK);
    }

}
