package co.yixiang.modules.user.rest;

import co.yixiang.modules.logging.aop.log.Log;
import co.yixiang.modules.user.param.AddressParam;
import co.yixiang.modules.user.service.YxUserAddressService;
import co.yixiang.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author hupeng
 * @date 2019-11-06
 */
@Api(tags = "APP商城:地址管理")
@RestController
@RequestMapping("/app/api/address")
public class AUserAddressController {

    private final YxUserAddressService yxUserAddressService;


    public AUserAddressController(YxUserAddressService yxUserAddressService){
        this.yxUserAddressService = yxUserAddressService;
    }
    /**
     * 设置默认地址
     * @param addressId 地址id
     */
    @Log("设置默认地址")
    @ApiOperation(value = "设置默认地址")
    @PutMapping(value = "/setDefault/{addressId}")
    public ResponseEntity setDefault(@PathVariable("addressId") Long addressId){
        yxUserAddressService.setDefault(SecurityUtils.getUserId(), addressId);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    /**
     * 添加或者修改地址
     * @param param AddressParam
     */
    @Log("添加或者修改地址")
    @ApiOperation(value = "添加或者修改地址")
    @PostMapping(value = "/addAndEdit")
    public ResponseEntity addAndEdit(@RequestBody AddressParam param){
        return new ResponseEntity<>(yxUserAddressService.addAndEdit(SecurityUtils.getUserId(),param), HttpStatus.OK);

    }

    /**
     * 地址详情
     * @param id 地址id
     * @return YxUserAddressQueryVo
     */
    @ApiOperation(value = "地址详情")
    @PostMapping(value = "/getDetail")
    public ResponseEntity getDetail(Long id){
        return new ResponseEntity<>(yxUserAddressService.getDetail(id), HttpStatus.OK);
    }

    /**
     * 获取用户地址
     * @param page page
     * @param limit limit
     * @return List
     */
    @ApiOperation(value = "地址详情")
    @GetMapping(value = "/getDetail")
    public ResponseEntity getList(int page, int limit){
        return new ResponseEntity<>(yxUserAddressService.getList(SecurityUtils.getUserId(), page, limit), HttpStatus.OK);
    }

    /**
     * 获取默认地址
     * @return YxUserAddress
     */
    @ApiOperation(value = "获取默认地址")
    @GetMapping("/getUserDefaultAddress")
    public ResponseEntity getUserDefaultAddress() {
        return new ResponseEntity<>(yxUserAddressService.getUserDefaultAddress(SecurityUtils.getUserId()), HttpStatus.OK);
    }
}
